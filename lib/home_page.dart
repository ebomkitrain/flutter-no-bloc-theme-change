import 'package:flutter/material.dart';
import 'package:theme_changer_no_bloc/config.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          currentTheme.switchTheme();
        },
        label: Text('Switch'),
        icon: Icon(Icons.brightness_high),
      ),
    );
  }
}
